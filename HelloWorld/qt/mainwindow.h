#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QGLWidget;
class AppDelegate;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
    void setCocosAppDelegate(AppDelegate *appDelegate);
    QWidget* getGLViewSuperWidget(void);

protected:
    void closeEvent(QCloseEvent *);

private slots:

private:
    Ui::MainWindow *ui;
    AppDelegate *m_appDelegate;
    QWidget *m_glWidget;
};

#endif // MAINWINDOW_H
