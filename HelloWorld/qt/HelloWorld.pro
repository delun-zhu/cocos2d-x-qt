QT       += core gui opengl

TARGET = HelloWorld
TEMPLATE = app

COCOSMODULES += lua cocosdenshion extensions
include(../../lib/proj.qt/libcocos2d-header.pri)

CONFIG(debug, debug|release) {
    DESTDIR = $$PWD/../../Debug.qt
    DEFINES += COCOS2D_DEBUG=1
}

CONFIG(release, debug|release) {
    DESTDIR = $$PWD/../../Release.qt
}

win32 {
    LIBS += -L$(DESTDIR) -llibcocos2d
    CONFIG += console
}

unix {
    LIBS += -L$(DESTDIR) -lcocos2d
}

unix:macx {
    CONFIG -= app_bundle
}

INCLUDEPATH += \
        ../Classes \
        .

HEADERS += \
    ../Classes/HelloWorldScene.h \
    ../Classes/AppDelegate.h \
    MainWindow.h


SOURCES += \
    ../Classes/HelloWorldScene.cpp \
    ../Classes/AppDelegate.cpp \
    main.cpp \
    MainWindow.cpp

FORMS += \
    MainWindow.ui

OTHER_FILES += \
    $$(PWD)/../Resources/Pea.png

