#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cocos2d.h>

// Qt
#include <QHBoxLayout>
#include <QDebug>
#include <QTimer>
#include <QGLWidget>
#include <QLabel>
#include <QVBoxLayout>

#include "AppDelegate.h"
#include "HelloWorldScene.h"


using namespace cocos2d;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_appDelegate(NULL)
    , m_glWidget(NULL)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *)
{
    cocos2d::CCDirector::sharedDirector()->end();
    qApp->quit();
}

void MainWindow::setCocosAppDelegate(AppDelegate *appDelegate)
{
    m_appDelegate = appDelegate;
}

QWidget*
MainWindow::getGLViewSuperWidget(void)
{
    return ui->mGLViewParentWidget;
}
