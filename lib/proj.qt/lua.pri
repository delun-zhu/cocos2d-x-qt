INCLUDEPATH += \
    ../cocos2d-x/scripting/lua/lua \
    ../cocos2d-x/scripting/lua/tolua \
    ../cocos2d-x/scripting/lua/cocos2dx_support

SOURCES += \
    ../cocos2d-x/scripting/lua/lua/lapi.c \
    ../cocos2d-x/scripting/lua/lua/lauxlib.c \
    ../cocos2d-x/scripting/lua/lua/lbaselib.c \
    ../cocos2d-x/scripting/lua/lua/lcode.c \
    ../cocos2d-x/scripting/lua/lua/ldblib.c \
    ../cocos2d-x/scripting/lua/lua/ldebug.c \
    ../cocos2d-x/scripting/lua/lua/ldo.c \
    ../cocos2d-x/scripting/lua/lua/ldump.c \
    ../cocos2d-x/scripting/lua/lua/lfunc.c \
    ../cocos2d-x/scripting/lua/lua/lgc.c \
    ../cocos2d-x/scripting/lua/lua/linit.c \
    ../cocos2d-x/scripting/lua/lua/liolib.c \
    ../cocos2d-x/scripting/lua/lua/llex.c \
    ../cocos2d-x/scripting/lua/lua/lmathlib.c \
    ../cocos2d-x/scripting/lua/lua/lmem.c \
    ../cocos2d-x/scripting/lua/lua/loadlib.c \
    ../cocos2d-x/scripting/lua/lua/lobject.c \
    ../cocos2d-x/scripting/lua/lua/lopcodes.c \
    ../cocos2d-x/scripting/lua/lua/loslib.c \
    ../cocos2d-x/scripting/lua/lua/lparser.c \
    ../cocos2d-x/scripting/lua/lua/lstate.c \
    ../cocos2d-x/scripting/lua/lua/lstring.c \
    ../cocos2d-x/scripting/lua/lua/lstrlib.c \
    ../cocos2d-x/scripting/lua/lua/ltable.c \
    ../cocos2d-x/scripting/lua/lua/ltablib.c \
    ../cocos2d-x/scripting/lua/lua/ltm.c \
    ../cocos2d-x/scripting/lua/lua/lua.c \
    ../cocos2d-x/scripting/lua/lua/lundump.c \
    ../cocos2d-x/scripting/lua/lua/lvm.c \
    ../cocos2d-x/scripting/lua/lua/lzio.c \
    ../cocos2d-x/scripting/lua/lua/print.c \
    ../cocos2d-x/scripting/lua/tolua/tolua_event.c \
    ../cocos2d-x/scripting/lua/tolua/tolua_is.c \
    ../cocos2d-x/scripting/lua/tolua/tolua_map.c \
    ../cocos2d-x/scripting/lua/tolua/tolua_push.c \
    ../cocos2d-x/scripting/lua/tolua/tolua_to.c \
    ../cocos2d-x/scripting/lua/cocos2dx_support/CCLuaBridge.cpp \
    ../cocos2d-x/scripting/lua/cocos2dx_support/CCLuaEngine.cpp \
    ../cocos2d-x/scripting/lua/cocos2dx_support/CCLuaStack.cpp \
    ../cocos2d-x/scripting/lua/cocos2dx_support/CCLuaValue.cpp \
    ../cocos2d-x/scripting/lua/cocos2dx_support/Cocos2dxLuaLoader.cpp \
    ../cocos2d-x/scripting/lua/cocos2dx_support/LuaCocos2d.cpp \
    ../cocos2d-x/scripting/lua/cocos2dx_support/snapshot.c \
    ../cocos2d-x/scripting/lua/cocos2dx_support/tolua_fix.c
